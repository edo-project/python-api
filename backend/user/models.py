from django.db import models

# Create your models here.
class User(models.Model):
    id_user = models.AutoField(primary_key=True)
    username = models.CharField(max_length=250)
    password = models.CharField(max_length=250)
    email = models.CharField(max_length=250)
    fullname = models.CharField(max_length=250)
    phone = models.CharField(max_length=250)

def __str__(self):
    return self.id_user




