from rest_framework import serializers
from .models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields = "__all__"

        ## if u get all column user :
        ## fields = "__all__"
